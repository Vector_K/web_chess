// ----------------------------- Начальная расстановка -----------------------------

const board = [
    1, 2, 3, 4, 5, 6, 7, 8,
    9, 10, 11, 12, 13, 14, 15, 16,
    null, null, null, null, null, null, null, null,
    null, null, null, null, null, null, null, null,
    null, null, null, null, null, null, null, null,
    null, null, null, null, null, null, null, null,
    17, 18, 19, 20, 21, 22, 23, 24,
    25, 26, 27, 28, 29, 30, 31, 32
];



// ----------------------------- Формирование страницы -----------------------------

let content = document.createElement('div'); // Страница
content.className = "content";


let buttonPanel = document.createElement('div'); // Панель кнопок
buttonPanel.className = "buttons";

let rulesButton = document.createElement('button'); // Кнопка правил
rulesButton.className = "rules";
rulesButton.textContent = "Правила";
buttonPanel.appendChild(rulesButton);

let restartButton = document.createElement('button'); // Кнопка рестарта
restartButton.className = "restart";
restartButton.textContent = "Рестарт";
restartButton.onclick = function() { restartGame() };
buttonPanel.appendChild(restartButton);


let playArea = document.createElement('div'); // Зона для игры
playArea.className = "playArea";

let table = document.createElement('table'); // Таблица (шахматная доска)
let ids = 0;

for (let i = 0; i < 8; i++) {
	let row = table.insertRow(); // Вставка строки
	for (let j = 0; j < 8; j++) {
		let cell = row.insertCell(); // Вставка столбца
        if (i % 2 == 0) { // Определение цвета ячейки
            if (j % 2 == 0)
                cell.id = "white";
            else
                cell.id = "black";
        }
        else {
            if (j % 2 != 0)
                cell.id = "white";
            else
                cell.id = "black";
        }
        if (board[ids] != null) { // Начальная расстановка фигур
            cell.className = board[ids];
        }
        else
            cell.className = 0;
        ids++;
	}
}

let player2 = document.createElement('div');
player2.className = "player2";
let pl2Text = document.createElement('div');
pl2Text.className = "pl2Text";
let wFigs = document.createElement('div');
wFigs.className = "wFigs";
let w1 = document.createElement('div');
let w2 = document.createElement('div');
w1.className = "w1";
w2.className = "w2";
wFigs.appendChild(w1);
wFigs.appendChild(w2);
player2.appendChild(pl2Text);
player2.appendChild(wFigs);

let player1 = document.createElement('div');
player1.className = "player1";
let pl1Text = document.createElement('div');
pl1Text.className = "pl1Text";
let bFigs = document.createElement('div');
bFigs.className = "bFigs";
let b1 = document.createElement('div');
let b2 = document.createElement('div');
b1.className = "b1";
b2.className = "b2";
bFigs.appendChild(b1);
bFigs.appendChild(b2);
player1.appendChild(pl1Text);
player1.appendChild(bFigs);

playArea.appendChild(player2);
playArea.appendChild(table);
playArea.appendChild(player1);


content.appendChild(buttonPanel);
content.appendChild(playArea);
document.body.appendChild(content);



// ----------------------------- Фигуры -----------------------------

class pawn { //  Пешка
    constructor(color) {
        this.color = color;
        this.figures = [];
        if (this.color == "white") { // Фигуры в зависимости от цвета
            for (let i = 17; i <= 24; i++)
                this.figures.push(document.getElementsByClassName(`${i}`));
        }
        else {
            for (let i = 9; i <= 16; i++)
                this.figures.push(document.getElementsByClassName(`${i}`));
        }
    }

    update() { // Обновление информации о фигурах
        if (turn) {
            let x = 0;
            for (let i = 17; i <= 24; i++) {
                this.figures[x] = document.getElementsByClassName(`${i}`);
                x++;
            }
        }
        else {
            let x = 0;
            for (let i = 9; i <= 16; i++) {
                this.figures[x] = document.getElementsByClassName(`${i}`);
                x++;
            }
        }
    }

    check(figID) { // Проверка на допустимость хода
        if (selectedFig == figID) { // Если уже выбрано, то убрать из выбранного
            removeChecked();
        }
        else { // Если не выбрано, то продолжить проверку
            removeChecked();
            selectedFig = figID;
            let i = 0;
            for (i; i < cells.length; i++) { // Поиск ячейки с фигурой
                if (cells[i].classList.contains(figID))
                    break;
            }
            let mas = [];
            let limit;
            if (this.color == "white") {
                if (i >= 48 && i <= 55) // Если ход из начальной позиции
                    limit = i - 16;
                else
                    limit = i - 8;
                for (let j = i - 8; j >= limit; j = j - 8) { // Поиск допустимых ходов
                    if (j < 0)
                        break;
                    if (cells[j].classList.contains(0)) {
                        if (KingW.shahCheck(0, kW, j) == -1) // Проверка на шах
                            mas.push(j);
                    }
                    else
                        break;
                }
                for (let j = 1; j <= 16; j++) { // Если на диагонали есть фигура противник
                    if (i != 0 && i != 8 && i != 16 && i != 24 && i != 32 && i != 40 && i != 48 && i != 56 && cells[i - 9].classList.contains(j))
                        mas.push(i - 9);
                    if (i != 7 && i != 15 && i != 23 && i != 31 && i != 39 && i != 47 && i != 55 && i != 63 && cells[i - 7].classList.contains(j))
                        mas.push(i - 7);
                }
            }
            else {
                if (i >= 8 && i <= 15) // Если ход из начальной позиции
                    limit = i + 16;
                else
                    limit = i + 8;
                for (let j = i + 8; j <= limit; j = j + 8) { // Поиск допустимых ходов
                    if (j > 63)
                        break;
                    if (cells[j].classList.contains(0)) {
                        if (KingB.shahCheck(0, kB, j) == -1) // Проверка на шах
                            mas.push(j);
                    }
                    else
                        break;
                }
                for (let j = 17; j <= 32; j++) { // Если на диагонали есть фигура противник
                    if (i != 7 && i != 15 && i != 23 && i != 31 && i != 39 && i != 47 && i != 55 && i != 63 && cells[i + 9].classList.contains(j))
                        mas.push(i + 9);
                    if (i != 0 && i != 8 && i != 16 && i != 24 && i != 32 && i != 40 && i != 48 && i != 56 && cells[i + 7].classList.contains(j))
                        mas.push(i + 7);
                }
            }
            checkTD(mas);
            giveMoveEvent(); // Ожидание нажатия для передвижения
        }
    }
}



class rook { // Ладья
    constructor(color) {
        this.color = color;
        this.figures = [];
        if (this.color == "white") { // Фигуры в зависимости от цвета
            this.figures.push(document.getElementsByClassName('25'));
            this.figures.push(document.getElementsByClassName('32'));
        }
        else {
            this.figures.push(document.getElementsByClassName('1'));
            this.figures.push(document.getElementsByClassName('8'));
        }
    }

    update() { // Обновление информации о фигурах
        if (turn) {
            this.figures[0] = (document.getElementsByClassName('25'));
            this.figures[1] = (document.getElementsByClassName('32'));
        }
        else {
            this.figures[0] = (document.getElementsByClassName('1'));
            this.figures[1] = (document.getElementsByClassName('8'));
        }
    }

    check(figID) { // Проверка на допустимость хода
        if (selectedFig == figID) { // Если уже выбрано, то убрать из выбранного
            removeChecked();
        }
        else { // Если не выбрано, то продолжить проверку
            removeChecked();
            selectedFig = figID;
            let i = 0;
            for (i; i < cells.length; i++) { // Поиск ячейки с фигурой
                if (cells[i].classList.contains(figID))
                    break;
            }
            let mas = [];
            mas = checkForMovs(mas, i - 8, 0, this.color, 1, 8, 0, 0, -1); // Вверх
            mas = checkForMovs(mas, i + 8, 63, this.color, 0, 8, 0, 0, -1); // Вниз
            let lBorder = 0;
            let rBorder = 7;
            if (i >= 8 && i <= 15) {
                lBorder = 8;
                rBorder = 15;
            }
            if (i >= 16 && i <= 23) {
                lBorder = 16;
                rBorder = 23;
            }
            if (i >= 24 && i <= 31) {
                lBorder = 24;
                rBorder = 31;
            }
            if (i >= 32 && i <= 39) {
                lBorder = 32;
                rBorder = 39;
            }
            if (i >= 40 && i <= 47) {
                lBorder = 40;
                rBorder = 47;
            }
            if (i >= 48 && i <= 55) {
                lBorder = 48;
                rBorder = 55;
            }
            if (i >= 56 && i <= 63) {
                lBorder = 56;
                rBorder = 63;
            }
            mas = checkForMovs(mas, i - 1, lBorder, this.color, 1, 1, 0, 0, -1); // Влево
            mas = checkForMovs(mas, i + 1, rBorder, this.color, 0, 1, 0, 0, -1); // Вправо
            checkTD(mas);
            giveMoveEvent(); // Ожидание нажатия для передвижения
        }
    }
}



class knight { // Конь
    constructor(color) {
        this.color = color;
        this.figures = [];
        if (this.color == "white") { // Фигуры в зависимости от цвета
            this.figures.push(document.getElementsByClassName('26'));
            this.figures.push(document.getElementsByClassName('31'));
        }
        else {
            this.figures.push(document.getElementsByClassName('2'));
            this.figures.push(document.getElementsByClassName('7'));
        }
    }

    update() { // Обновление информации о фигурах
        if (turn) {
            this.figures[0] = (document.getElementsByClassName('26'));
            this.figures[1] = (document.getElementsByClassName('31'));
        }
        else {
            this.figures[0] = (document.getElementsByClassName('2'));
            this.figures[1] = (document.getElementsByClassName('7'));
        }
    }

    check(figID) { // Проверка на допустимость хода
        if (selectedFig == figID) { // Если уже выбрано, то убрать из выбранного
            removeChecked();
        }
        else { // Если не выбрано, то продолжить проверку
            removeChecked();
            selectedFig = figID;
            let i = 0;
            for (i; i < cells.length; i++) { // Поиск ячейки с фигурой
                if (cells[i].classList.contains(figID))
                    break;
            }
            let mas = [];
            if (i > 16 && i != 24 && i != 32 && i != 40 && i != 48 && i != 56) // Вверх - влево
                mas = checkForMovs(mas, i - 17, i - 17, this.color, 1, 1, 0, 0, -1);
            if (i > 16 && i != 23 && i != 31 && i != 39 && i != 47 && i != 55 && i != 63) // Вверх - вправо
                mas = checkForMovs(mas, i - 15, i - 15, this.color, 1, 1, 0, 0, -1);
            if (i > 8 && i != 14 && i != 15 && i != 22 && i != 23 && i != 30 && i != 31 && i != 38 && i != 39 && i != 46 && i != 47 && i != 54 && i != 55 && i != 62 && i != 63) // Вправо - вверх
                mas = checkForMovs(mas, i - 6, i - 6, this.color, 1, 1, 0, 0, -1);
            if (i < 56 && i != 14 && i != 15 && i != 22 && i != 23 && i != 30 && i != 31 && i != 38 && i != 39 && i != 46 && i != 47 && i != 54 && i != 55 && i != 6 && i != 7) // Вправо - вниз
                mas = checkForMovs(mas, i + 10, i + 10, this.color, 0, 1, 0, 0, -1);
            if (i < 47 && i != 7 && i != 15 && i != 23 && i != 31 && i != 39) // Вниз - вправо
                mas = checkForMovs(mas, i + 17, i + 17, this.color, 0, 1, 0, 0, -1);
            if (i < 48 && i != 0 && i != 8 && i != 16 && i != 24 && i != 32 && i != 40) // Вниз - влево
                mas = checkForMovs(mas, i + 15, i + 15, this.color, 0, 1, 0, 0, -1);
            if (i < 56 && i != 8 && i != 9 && i != 16 && i != 17 && i != 0 && i != 1 && i != 24 && i != 25 && i != 32 && i != 33 && i != 40 && i != 41 && i != 48 && i != 49) // Влево - вниз
                mas = checkForMovs(mas, i + 6, i + 6, this.color, 0, 1, 0, 0, -1);
            if (i > 9 && i != 16 && i != 17 && i != 24 && i != 25 && i != 32 && i != 33 && i != 40 && i != 41 && i != 48 && i != 49 && i != 56 && i != 57) // Влево - вверх
                mas = checkForMovs(mas, i - 10, i - 10, this.color, 1, 1, 0, 0, -1);
            checkTD(mas);
            giveMoveEvent(); // Ожидание нажатия для передвижения
        }
    }
}



class bishop { // Офицер
    constructor(color) {
        this.color = color;
        this.figures = [];
        if (this.color == "white") { // Фигуры в зависимости от цвета
            this.figures.push(document.getElementsByClassName('27'));
            this.figures.push(document.getElementsByClassName('30'));
        }
        else {
            this.figures.push(document.getElementsByClassName('3'));
            this.figures.push(document.getElementsByClassName('6'));
        }
    }

    update() { // Обновление информации о фигурах
        if (turn) {
            this.figures[0] = (document.getElementsByClassName('27'));
            this.figures[1] = (document.getElementsByClassName('30'));
        }
        else {
            this.figures[0] = (document.getElementsByClassName('3'));
            this.figures[1] = (document.getElementsByClassName('6'));
        }
    }

    check(figID) { // Проверка на допустимость хода
        if (selectedFig == figID) { // Если уже выбрано, то убрать из выбранного
            removeChecked();
        }
        else { // Если не выбрано, то продолжить проверку
            removeChecked();
            selectedFig = figID;
            let i = 0;
            for (i; i < cells.length; i++) { // Поиск ячейки с фигурой
                if (cells[i].classList.contains(figID))
                    break;
            }
            let mas = [];
            if (i != 0 && i != 8 && i != 16 && i != 24 && i != 32 && i != 40 && i != 48 && i != 56 && i != 1 && i != 2 && i != 3 && i != 4 && i != 5 && i != 6 && i != 7)
                mas = checkForMovs(mas, i - 9, 0, this.color, 1, 9, 1, 0, -1); // Вверх - влево
            if (i != 7 && i != 15 && i != 23 && i != 31 && i != 39 && i != 47 && i != 55 && i != 63 && i != 1 && i != 2 && i != 3 && i != 4 && i != 5 && i != 6 && i != 0)
                mas = checkForMovs(mas, i - 7, 0, this.color, 1, 7, 1, 0, -1); // Вверх - вправо
            if (i != 7 && i != 15 && i != 23 && i != 31 && i != 39 && i != 47 && i != 55 && i != 63 && i != 56 && i != 57 && i != 58 && i != 59 && i != 60 && i != 61 && i != 62)
                mas = checkForMovs(mas, i + 9, 63, this.color, 0, 9, 1, 0, -1); // Вниз - вправо
            if (i != 0 && i != 8 && i != 16 && i != 24 && i != 32 && i != 40 && i != 48 && i != 56 && i != 63 && i != 57 && i != 58 && i != 59 && i != 60 && i != 61 && i != 62)
                mas = checkForMovs(mas, i + 7, 63, this.color, 0, 7, 1, 0, -1); // Вниз - влево
            checkTD(mas);
            giveMoveEvent(); // Ожидание нажатия для передвижения
        }
    }
}



class queen { // Ферзь
    constructor(color) {
        this.color = color;
        this.figures = [];
        if (this.color == "white") // Фигуры в зависимости от цвета
            this.figures.push(document.getElementsByClassName('28'));
        else
            this.figures.push(document.getElementsByClassName('4'));
    }

    update() { // Обновление информации о фигурах
        if (turn)
            this.figures[0] = (document.getElementsByClassName('28'));
        else
            this.figures[0] = (document.getElementsByClassName('4'));
    }

    check(figID) { // Проверка на допустимость хода
        if (selectedFig == figID) { // Если уже выбрано, то убрать из выбранного
            removeChecked();
        }
        else { // Если не выбрано, то продолжить проверку
            removeChecked();
            selectedFig = figID;
            let i = 0;
            for (i; i < cells.length; i++) { // Поиск ячейки с фигурой
                if (cells[i].classList.contains(figID))
                    break;
            }
            let mas = [];
            if (i != 0 && i != 8 && i != 16 && i != 24 && i != 32 && i != 40 && i != 48 && i != 56 && i != 1 && i != 2 && i != 3 && i != 4 && i != 5 && i != 6 && i != 7)
                mas = checkForMovs(mas, i - 9, 0, this.color, 1, 9, 1, 0, -1); // Вверх - влево
            if (i != 7 && i != 15 && i != 23 && i != 31 && i != 39 && i != 47 && i != 55 && i != 63 && i != 1 && i != 2 && i != 3 && i != 4 && i != 5 && i != 6 && i != 0)
                mas = checkForMovs(mas, i - 7, 0, this.color, 1, 7, 1, 0, -1); // Вверх - вправо
            if (i != 7 && i != 15 && i != 23 && i != 31 && i != 39 && i != 47 && i != 55 && i != 63 && i != 56 && i != 57 && i != 58 && i != 59 && i != 60 && i != 61 && i != 62)
                mas = checkForMovs(mas, i + 9, 63, this.color, 0, 9, 1, 0, -1); // Вниз - вправо
            if (i != 0 && i != 8 && i != 16 && i != 24 && i != 32 && i != 40 && i != 48 && i != 56 && i != 63 && i != 57 && i != 58 && i != 59 && i != 60 && i != 61 && i != 62)
                mas = checkForMovs(mas, i + 7, 63, this.color, 0, 7, 1, 0, -1); // Вниз - влево
            mas = checkForMovs(mas, i - 8, 0, this.color, 1, 8, 0, 0, -1); // Вверх
            mas = checkForMovs(mas, i + 8, 63, this.color, 0, 8, 0, 0, -1); // Вниз
            let lBorder = 0;
            let rBorder = 7;
            if (i >= 8 && i <= 15) {
                lBorder = 8;
                rBorder = 15;
            }
            if (i >= 16 && i <= 23) {
                lBorder = 16;
                rBorder = 23;
            }
            if (i >= 24 && i <= 31) {
                lBorder = 24;
                rBorder = 31;
            }
            if (i >= 32 && i <= 39) {
                lBorder = 32;
                rBorder = 39;
            }
            if (i >= 40 && i <= 47) {
                lBorder = 40;
                rBorder = 47;
            }
            if (i >= 48 && i <= 55) {
                lBorder = 48;
                rBorder = 55;
            }
            if (i >= 56 && i <= 63) {
                lBorder = 56;
                rBorder = 63;
            }
            mas = checkForMovs(mas, i - 1, lBorder, this.color, 1, 1, 0, 0, -1); // Влево
            mas = checkForMovs(mas, i + 1, rBorder, this.color, 0, 1, 0, 0, -1); // Вправо
            checkTD(mas);
            giveMoveEvent(); // Ожидание нажатия для передвижения
        }
    }
}


class king { // Король
    constructor(color) {
        this.color = color;
        this.figures = [];
        if (this.color == "white") // Фигуры в зависимости от цвета
            this.figures.push(document.getElementsByClassName('29'));
        else
            this.figures.push(document.getElementsByClassName('5'));
    }

    update() { // Обновление информации о фигурах
        if (turn)
            this.figures[0] = (document.getElementsByClassName('29'));
        else
            this.figures[0] = (document.getElementsByClassName('5'));
    }

    check(figID) { // Проверка на допустимость хода
        if (selectedFig == figID) { // Если уже выбрано, то убрать из выбранного
            removeChecked();
        }
        else { // Если не выбрано, то продолжить проверку
            removeChecked();
            selectedFig = figID;
            let i = 0;
            for (i; i < cells.length; i++) { // Поиск ячейки с фигурой
                if (cells[i].classList.contains(figID))
                    break;
            }
            let mas = [];
            let tempFS = figShah;
            let tempKP;
            if (this.color == "white")
                tempKP = kW;
            else
                tempKP = kB;
            if (i - 8 >= 0 && this.shahCheck(0, i - 8, -1) == -1) { // Вверх
                if (this.color == "white")
                    kW = i - 8;
                else
                    kB = i - 8;
                mas = checkForMovs(mas, i - 8, i - 8, this.color, 1, 1, 0, 0, -1);
            }
            if (i + 8 <= 63 && this.shahCheck(0, i + 8, -1) == -1) { // Вниз
                if (this.color == "white")
                    kW = i + 8;
                else
                    kB = i + 8;
                mas = checkForMovs(mas, i + 8, i + 8, this.color, 0, 1, 0, 0, -1);
            }
            if (i != 7 && i != 15 && i != 23 && i != 31 && i != 39 && i != 47 && i != 55 && i != 63 && this.shahCheck(0, i + 1, -1) == -1) { // Вправо
                if (this.color == "white")
                    kW = i + 1;
                else
                    kB = i + 1;
                mas = checkForMovs(mas, i + 1, i + 1, this.color, 0, 1, 0, 0, -1);
            }
            if (i != 0 && i != 8 && i != 16 && i != 24 && i != 32 && i != 40 && i != 48 && i != 56 && this.shahCheck(0, i - 1, -1) == -1) { // Влево
                if (this.color == "white")
                    kW = i - 1;
                else
                    kB = i - 1;
                mas = checkForMovs(mas, i - 1, i - 1, this.color, 1, 1, 0, 0, -1);
            }
            if (i - 9 >= 0 && i != 0 && i != 8 && i != 16 && i != 24 && i != 32 && i != 40 && i != 48 && i != 56 && this.shahCheck(0, i - 9, -1) == -1) { // Вверх - влево
                if (this.color == "white")
                    kW = i - 9;
                else
                    kB = i - 9;
                mas = checkForMovs(mas, i - 9, i - 9, this.color, 1, 1, 0, 0, -1);
            }
            if (i - 7 >= 0 && i != 7 && i != 15 && i != 23 && i != 31 && i != 39 && i != 47 && i != 55 && i != 63 && this.shahCheck(0, i - 7, -1) == -1) { // Вверх - вправо
                if (this.color == "white")
                    kW = i - 7;
                else
                    kB = i - 7;
                mas = checkForMovs(mas, i - 7, i - 7, this.color, 1, 1, 0, 0, -1);
            }
            if (i + 7 <= 63 && i != 0 && i != 8 && i != 16 && i != 24 && i != 32 && i != 40 && i != 48 && i != 56 && this.shahCheck(0, i + 7, -1) == -1) { // Вниз - влево
                if (this.color == "white")
                    kW = i + 7;
                else
                    kB = i + 7;
                mas = checkForMovs(mas, i + 7, i + 7, this.color, 0, 1, 0, 0, -1);
            }
            if (i + 9 <= 63 && i != 7 && i != 15 && i != 23 && i != 31 && i != 39 && i != 47 && i != 55 && i != 63 && this.shahCheck(0, i + 9, -1) == -1) { // Вниз - вправо
                if (this.color == "white")
                    kW = i + 9;
                else
                    kB = i + 9;
                mas = checkForMovs(mas, i + 9, i + 9, this.color, 0, 1, 0, 0, -1);
            }
            figShah = tempFS;
            if (this.color == "white")
                kW = tempKP;
            else
                kB = tempKP;
            checkTD(mas);
            giveMoveEvent(); // Ожидание нажатия для передвижения
        }
    }

    shahCheck(figID, turnCell, turnPos) {
        let i = 0;
        if (turnCell > -1) // Если проверка вызвана при ходе короля
            i = turnCell;
        else { // Если проверка вызвана после хода игрока
            for (i; i < cells.length; i++) { // Поиск ячейки с фигурой
                if (cells[i].classList.contains(figID))
                    break;
            }
        }
        let x = 0;
        if (this.color == "white") {
            if (i > 7 && i != 15 && i != 23 && i != 31 && i != 39 && i != 47 && i != 55 && i != 63 && cells[i - 7].className >= 9 && cells[i - 7].className <= 16)
                x = cells[i - 7].className; // Пешка
            if (i > 8 && i != 16 && i != 24 && i != 32 && i != 40 && i != 48 && i != 56 && cells[i - 9].className >= 9 && cells[i - 9].className <= 16)
                x = cells[i - 9].className; // Пешка
            
        }
        else {
            if (i < 55 && i != 7 && i != 15 && i != 23 && i != 31 && i != 39 && i != 47 && cells[i + 9].className >= 17 && cells[i + 9].className <= 24)
                x = cells[i + 9].className; // Пешка
            if (i < 56 && i != 0 && i != 8 && i != 16 && i != 24 && i != 32 && i != 40 && i != 48 && cells[i + 7].className >= 17 && cells[i + 7].className <= 24)
                x = cells[i + 7].className; // Пешка
        }
        if (x > 0) {
            figShah = x;
            return i;
        }
        else
            x = 0;
        if (i != 0 && i != 8 && i != 16 && i != 24 && i != 32 && i != 40 && i != 48 && i != 56 && i != 1 && i != 2 && i != 3 && i != 4 && i != 5 && i != 6 && i != 7)
            x = checkForMovs(x, i - 9, 0, this.color, 1, 9, 1, 1, turnPos); // Вверх - влево
        if (x == 3 || x == 4 || x == 6 || x == 27 || x == 28 || x == 30) {
            figShah = x;
            return i;
        }
        else
            x = 0;
        if (i != 7 && i != 15 && i != 23 && i != 31 && i != 39 && i != 47 && i != 55 && i != 63 && i != 1 && i != 2 && i != 3 && i != 4 && i != 5 && i != 6 && i != 0)
            x = checkForMovs(x, i - 7, 0, this.color, 1, 7, 1, 1, turnPos); // Вверх - вправо
        if (x == 3 || x == 4 || x == 6 || x == 27 || x == 28 || x == 30) {
            figShah = x;
            return i;
        }
        else
            x = 0;
        if (i != 7 && i != 15 && i != 23 && i != 31 && i != 39 && i != 47 && i != 55 && i != 63 && i != 56 && i != 57 && i != 58 && i != 59 && i != 60 && i != 61 && i != 62)
            x = checkForMovs(x, i + 9, 63, this.color, 0, 9, 1, 1, turnPos); // Вниз - вправо
        if (x == 3 || x == 4 || x == 6 || x == 27 || x == 28 || x == 30) {
            figShah = x;
            return i;
        }
        else
            x = 0;
        if (i != 0 && i != 8 && i != 16 && i != 24 && i != 32 && i != 40 && i != 48 && i != 56 && i != 63 && i != 57 && i != 58 && i != 59 && i != 60 && i != 61 && i != 62)
            x = checkForMovs(x, i + 7, 63, this.color, 0, 7, 1, 1, turnPos); // Вниз - влево
        if (x == 3 || x == 4 || x == 6 || x == 27 || x == 28 || x == 30) {
            figShah = x;
            return i;
        }
        else
            x = 0;
        x = checkForMovs(x, i - 8, 0, this.color, 1, 8, 0, 1, turnPos); // Вверх
        if (x == 1 || x == 4 || x == 8 || x == 25 || x == 28 || x == 32) {
            figShah = x;
            return i;
        }
        else
            x = 0;
        x = checkForMovs(x, i + 8, 63, this.color, 0, 8, 0, 1, turnPos); // Вниз
        if (x == 1 || x == 4 || x == 8 || x == 25 || x == 28 || x == 32) {
            figShah = x;
            return i;
        }
        else
            x = 0;
        let lBorder = 0;
        let rBorder = 7;
        if (i >= 8 && i <= 15) {
            lBorder = 8;
            rBorder = 15;
        }
        if (i >= 16 && i <= 23) {
            lBorder = 16;
            rBorder = 23;
        }
        if (i >= 24 && i <= 31) {
            lBorder = 24;
            rBorder = 31;
        }
        if (i >= 32 && i <= 39) {
            lBorder = 32;
            rBorder = 39;
        }
        if (i >= 40 && i <= 47) {
            lBorder = 40;
            rBorder = 47;
        }
        if (i >= 48 && i <= 55) {
            lBorder = 48;
            rBorder = 55;
        }
        if (i >= 56 && i <= 63) {
            lBorder = 56;
            rBorder = 63;
        }
        x = checkForMovs(x, i - 1, lBorder, this.color, 1, 1, 0, 1, turnPos); // Влево
        if (x == 1 || x == 4 || x == 8 || x == 25 || x == 28 || x == 32) {
            figShah = x;
            return i;
        }
        else
            x = 0;
        x = checkForMovs(x, i + 1, rBorder, this.color, 0, 1, 0, 1, turnPos); // Вправо
        if (x == 1 || x == 4 || x == 8 || x == 25 || x == 28 || x == 32) {
            figShah = x;
            return i;
        }
        else
            x = 0;
        if (i > 16 && i != 24 && i != 32 && i != 40 && i != 48 && i != 56) // Вверх - влево
            x = checkForMovs(x, i - 17, i - 17, this.color, 1, 1, 0, 1, turnPos);
        if (x == 2 || x == 7 || x == 26 || x == 31) {
            figShah = x;
            return i;
        }
        else
            x = 0;
        if (i > 16 && i != 23 && i != 31 && i != 39 && i != 47 && i != 55 && i != 63) // Вверх - вправо
            x = checkForMovs(x, i - 15, i - 15, this.color, 1, 1, 0, 1, turnPos);
        if (x == 2 || x == 7 || x == 26 || x == 31) {
            figShah = x;
            return i;
        }
        else
            x = 0;
        if (i > 8 && i != 14 && i != 15 && i != 22 && i != 23 && i != 30 && i != 31 && i != 38 && i != 39 && i != 46 && i != 47 && i != 54 && i != 55 && i != 62 && i != 63) // Вправо - вверх
            x = checkForMovs(x, i - 6, i - 6, this.color, 1, 1, 0, 1, turnPos);
        if (x == 2 || x == 7 || x == 26 || x == 31) {
            figShah = x;
            return i;
        }
        else
            x = 0;
        if (i < 56 && i != 14 && i != 15 && i != 22 && i != 23 && i != 30 && i != 31 && i != 38 && i != 39 && i != 46 && i != 47 && i != 54 && i != 55 && i != 6 && i != 7) // Вправо - вниз
            x = checkForMovs(x, i + 10, i + 10, this.color, 0, 1, 0, 1, turnPos);
        if (x == 2 || x == 7 || x == 26 || x == 31) {
            figShah = x;
            return i;
        }
        else
            x = 0;
        if (i < 47 && i != 7 && i != 15 && i != 23 && i != 31 && i != 39) // Вниз - вправо
            x = checkForMovs(x, i + 17, i + 17, this.color, 0, 1, 0, 1, turnPos);
        if (x == 2 || x == 7 || x == 26 || x == 31) {
            figShah = x;
            return i;
        }
        else
            x = 0;
        if (i < 48 && i != 0 && i != 8 && i != 16 && i != 24 && i != 32 && i != 40) // Вниз - влево
            x = checkForMovs(x, i + 15, i + 15, this.color, 0, 1, 0, 1, turnPos);
        if (x == 2 || x == 7 || x == 26 || x == 31) {
            figShah = x;
            return i;
        }
        else
            x = 0;
        if (i < 56 && i != 8 && i != 9 && i != 16 && i != 17 && i != 0 && i != 1 && i != 24 && i != 25 && i != 32 && i != 33 && i != 40 && i != 41 && i != 48 && i != 49) // Влево - вниз
            x = checkForMovs(x, i + 6, i + 6, this.color, 0, 1, 0, 1, turnPos);
        if (x == 2 || x == 7 || x == 26 || x == 31) {
            figShah = x;
            return i;
        }
        else
            x = 0;
        if (i > 9 && i != 16 && i != 17 && i != 24 && i != 25 && i != 32 && i != 33 && i != 40 && i != 41 && i != 48 && i != 49 && i != 56 && i != 57) // Влево - вверх
            x = checkForMovs(x, i - 10, i - 10, this.color, 1, 1, 0, 1, turnPos);
        if (x == 2 || x == 7 || x == 26 || x == 31) {
            figShah = x;
            return i;
        }
        else
            x = 0;
        return -1;
    }
}

const PawnW = new pawn("white");
const PawnB = new pawn("black");
const RookW = new rook("white");
const RookB = new rook("black");
const KnightW = new knight("white");
const KnightB = new knight("black");
const BishopW = new bishop("white");
const BishopB = new bishop("black");
const QueenW = new queen("white");
const QueenB = new queen("black");
const KingW = new king("white");
const KingB = new king("black");

const whiteFig = [PawnW.figures, RookW.figures, KnightW.figures, BishopW.figures, QueenW.figures, KingW.figures];
const blackFig = [PawnB.figures, RookB.figures, KnightB.figures, BishopB.figures, QueenB.figures, KingB.figures];

const cells = document.querySelectorAll("td");

var selectedFig = 0; // Выбранная в данный момент фигура
var turn = 0; // Ход игрока
var eatenW = 0; // Съедено белых фигур
var eatenB = 0; // Съедено черных фигур
var isShah = 0; // Проверка на шах
var figShah = -1; // Номер фигуры, которая поставила шах
var kB = 4; // Ячейка, в которой находится черный король
var kW = 60; // Ячейка, в которой находится белый король



// ----------------------------- Функции -----------------------------

function givePiecesEventListeners() { // Выбор фигуры
    if (!turn)
        turn = 1;
    else
        turn = 0;
    if (turn) {
        if (KingW.shahCheck(parseInt(KingW.figures[0][0].className), -1, -1) > -1) { // Если шах
            isShah = 1;
            let text = document.createTextNode("ШАХ");
            pl1Text.appendChild(text);
        }
        else if (isShah == 1) {
            pl2Text.textContent = "";
            isShah = 0;
            figShah = -1;
        }
        for (let i = 0; i < whiteFig.length; i++) {
            for(let j = 0; j < whiteFig[i].length; j++) {
                if (whiteFig[i][j][0] != undefined) // Избежание ошибок, если фигуры нет на поле
                    whiteFig[i][j][0].addEventListener("click", getPlayerAvailableMoves);
            }
        }
    }
    else {
        if (KingB.shahCheck(parseInt(KingB.figures[0][0].className), -1, -1) > -1) { // Если шах
            isShah = 1;
            let text = document.createTextNode("ШАХ");
            pl2Text.appendChild(text);
        }
        else if (isShah == 1) {
            pl1Text.textContent = "";
            isShah = 0;
            figShah = -1;
        }
        for (let i = 0; i < blackFig.length; i++) {
            for(let j = 0; j < blackFig[i].length; j++) {
                if (blackFig[i][j][0] != undefined) // Избежание ошибок, если фигуры нет на поле
                    blackFig[i][j][0].addEventListener("click", getPlayerAvailableMoves);
            }
        }
    }
}

function getPlayerAvailableMoves(event) { // Получение доступных ходов
    if (turn) {
        if (event.target.className >= 17 && event.target.className <= 24)
            PawnW.check(parseInt(event.target.className));
        if (event.target.className == 25 || event.target.className == 32)
            RookW.check(parseInt(event.target.className));
        if (event.target.className == 26 || event.target.className == 31)
            KnightW.check(parseInt(event.target.className));
        if (event.target.className == 27 || event.target.className == 30)
            BishopW.check(parseInt(event.target.className));
        if (event.target.className == 28)
            QueenW.check(parseInt(event.target.className));
        if (event.target.className == 29)
            KingW.check(parseInt(event.target.className));
    } else {
        if (event.target.className >= 9 && event.target.className <= 16)
            PawnB.check(parseInt(event.target.className));
        if (event.target.className == 1 || event.target.className == 8)
            RookB.check(parseInt(event.target.className));
        if (event.target.className == 2 || event.target.className == 7)
            KnightB.check(parseInt(event.target.className));
        if (event.target.className == 3 || event.target.className == 6)
            BishopB.check(parseInt(event.target.className));
        if (event.target.className == 4)
            QueenB.check(parseInt(event.target.className));
        if (event.target.className == 5)
            KingB.check(parseInt(event.target.className));
    }
}

function checkForMovs(mas, y, z, color, be, dif, isDiag, sh, turnPos) { // Допустимость хода
    if (be) {
        for (y; y >= z; y = y - dif) { // Вверх
            if (y == turnPos)
                break;
            if (cells[y].className == selectedFig)
                continue;
            if (cells[y].classList.contains(0)) {
                if (!sh) {
                    if ((color == "white" && KingW.shahCheck(0, kW, y) == -1) || (color == "black" && KingB.shahCheck(0, kB, y) == -1)) // Проверка на шах
                        mas.push(y);
                }
            }
            else {
                if (isEnemyFig(color, y)) { // Если фигура противника
                    if (!sh)
                        mas.push(y);
                    else {
                        return cells[y].className;
                    }
                }
                break;
            }
            if (isDiag && (y == 7 || y == 15 || y == 23 || y == 31 || y == 39 || y == 47 || y == 55 || y == 63 || y == 0 || y == 8 || y == 16 || y == 24 || y == 32 || y == 40 || y == 48 || y == 56 || y == 1 || y == 2 || y == 3 || y == 4 || y == 5 || y == 6))
                break;
        }
    }
    else {
        for (y; y <= z; y = y + dif) { // Вниз
            if (y == turnPos)
                break;
            if (cells[y].className == selectedFig)
                continue;
            if (cells[y].classList.contains(0)) {
                if (!sh) {
                    if ((color == "white" && KingW.shahCheck(0, kW, y) == -1) || (color == "black" && KingB.shahCheck(0, kB, y) == -1)) // Проверка на шах
                        mas.push(y);
                }
            }
            else {
                if (isEnemyFig(color, y)) { // Если фигура противника
                    if (!sh)
                        mas.push(y);
                    else
                        return cells[y].className;
                }
                break;
            }
            if (isDiag && (y == 7 || y == 15 || y == 23 || y == 31 || y == 39 || y == 47 || y == 55 || y == 0 || y == 8 || y == 16 || y == 24 || y == 32 || y == 40 || y == 48 || y == 56 || y == 63 || y == 57 || y == 58 || y == 59 || y == 60 || y == 61 || y == 62))
                break;
        }
    }
    return mas;
}

function isEnemyFig(color, j) { // Если на пути фигура противника
    if (color == "white") {
        for (let i = 1; i <= 16; i++) {
            if (cells[j].classList.contains(i))
                return 1;
        }
        return 0;
    }
    else {
        for (let i = 17; i <= 32; i++) {
            if (cells[j].classList.contains(i))
                return 1;
        }
        return 0;
    }
}

function checkTD(mas) {
    let x = -1;
    for (let j = 0; j < mas.length; j++) {
        if (cells[mas[j]].className > 0)
            cells[mas[j]].classList.add('*');
        else {
            cells[mas[j]].classList.remove(0);
            cells[mas[j]].classList.add(x);
            x--;
        }
    }
}

function giveMoveEvent() { // Поиск ячеек для перемещения
    let x = [];
    for (let i = 1; i <= 32; i++) {
        let y = document.getElementsByClassName(`${i * -1}`);
        if (y[0] != null)
            x.push(y);
        let z = document.getElementsByClassName(`${i} *`);
        if (z[0] != null)
            x.push(z);
    }
    for (let i = 0; i < x.length; i++)
        x[i][0].addEventListener("click", movePlayerPieces);
}

function movePlayerPieces(event) { // Вызов функции для передвижения фигуры
    if (event.target.className < 0 || event.target.classList.contains('*'))
        move(parseInt(event.target.className));
}

function move(moveID) { // Передвижение
    for (let i = 0; i < cells.length; i++) { // Убрать фигуру из предыдущей позиции
        if (cells[i].classList.contains(selectedFig)) {
            cells[i].classList.remove(selectedFig);
            cells[i].classList.add(0);
        }
    }
    let trNum = 0;
    let tdNum = 0;
    for (let i = 0; i < cells.length; i++) { // Вычисление перемещения фигуры
        if (cells[i].classList.contains(moveID)) {
            if (selectedFig == 5)
                kB = i;
            if (selectedFig == 29)
                kW = i;
            break;
        }
        tdNum++;
        if (i == 7 || i == 15 || i == 23 || i == 31 || i == 39 || i == 47 || i == 55) {
            trNum++;
            tdNum = 0;
        }
    }
    let targetCell = document.getElementsByTagName('tr')[trNum].getElementsByTagName('td')[tdNum];
    targetCell.classList.remove(moveID);
    if (moveID > 0 && moveID != 5 && moveID != 29) { // Информация о съеденных фигурах
        let img = document.createElement('img');
        if (moveID < 17) {
            eatenB++;
            if (moveID < 17 && moveID > 8)
                img.setAttribute("src", "../img/PawnB.svg");
            else if (moveID == 1 || moveID == 8)
                img.setAttribute("src", "../img/RookB.svg");
            else if (moveID == 2 || moveID == 7)
                img.setAttribute("src", "../img/KnightB.svg");
            else if (moveID == 3 || moveID == 6)
                img.setAttribute("src", "../img/BishopB.svg");
            else if (moveID == 4)
                img.setAttribute("src", "../img/QueenB.svg");
            if (eatenB < 9)
                b2.appendChild(img);
            else
                b1.appendChild(img);
        }
        else {
            eatenW++;
            if (moveID < 25 && moveID > 16)
                img.setAttribute("src", "../img/PawnW.svg");
            else if (moveID == 25 || moveID == 32)
                img.setAttribute("src", "../img/RookW.svg");
            else if (moveID == 26 || moveID == 31)
                img.setAttribute("src", "../img/KnightW.svg");
            else if (moveID == 27 || moveID == 30)
                img.setAttribute("src", "../img/BishopW.svg");
            else if (moveID == 28)
                img.setAttribute("src", "../img/QueenW.svg");
            if (eatenW < 9)
                w2.appendChild(img);
            else
                w1.appendChild(img);
        }
    }
    targetCell.classList.add(selectedFig); // Перемещение фигуры
    if (turn) { // Обновление информации о фигурах
        if (selectedFig < 25 && selectedFig > 16)
            PawnW.update();
        if (selectedFig == 25 || selectedFig == 32)
            RookW.update();
        if (selectedFig == 26 || selectedFig == 31)
            KnightW.update();
        if (selectedFig == 27 || selectedFig == 30)
            BishopW.update();
        if (selectedFig == 28)
            QueenW.update();
        if (selectedFig == 29)
            KingW.update();
    } else {
        if (selectedFig < 17 && selectedFig > 8)
            PawnB.update();
        if (selectedFig == 1 || selectedFig == 8)
            RookB.update();
        if (selectedFig == 2 || selectedFig == 7)
            KnightB.update();
        if (selectedFig == 3 || selectedFig == 6)
            BishopB.update();
        if (selectedFig == 4)
            QueenB.update();
        if (selectedFig == 5)
            KingB.update();
    }
    removeChecked();
    givePiecesEventListeners();
}

function removeChecked() { // Очистить выбранные клетки
    for(let i = 0; i < cells.length; i++) {
        if (cells[i].classList < 0) {
            cells[i].classList.remove(cells[i].classList);
            cells[i].classList.add(0);
        }
        if (cells[i].className.includes('*'))
            cells[i].classList.remove('*');
    }
    selectedFig = 0;
    if (!isShah)
        figShah = -1;
}

function restartGame() { // Перезапуск игры
    for (let i = 0; i < cells.length; i++) { // Возврат к начальной установке
        cells[i].classList.remove(cells[i].classList);
        if (board[i] == null)
            cells[i].classList.add(0);
        else
            cells[i].classList.add(board[i]);
    }
    let img = document.getElementsByTagName('img');
    for (let i = img.length - 1; i >= 0; i--) // Очистка картинок съеденных фигур
        img[i].parentNode.removeChild(img[i]);
    selectedFig = 0;
    turn = 0;
    eatenB = 0;
    eatenW = 0;
    isShah = 0;
    kB = 4;
    kW = 60;
    pl1Text.textContent = "";
    pl2Text.textContent = "";
    givePiecesEventListeners();
}

givePiecesEventListeners();